#!/usr/bin/python3
import cgi

pedido = cgi.FieldStorage()

assinar = pedido.getvalue('assinar')
concordar = pedido.getvalue('concordar')

print('Content-type: text/html; charset=utf-8\n\n')

if assinar and assinar == 'sim':
    print('Obrigado por se tornar um membro!')

else:
    print('Até a próxima!')

print('<br>;)')

