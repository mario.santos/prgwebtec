#!/usr/bin/python3
import cgi

pedido = cgi.FieldStorage()

assinar = pedido.getvalue('assinar')

print('Content-type: text/html; charset=utf-8\n\n')

if assinar and assinar == 'sim':
    print('Já possui cadastro, mensagem de ativação em seu email')
else:
    print('Preencha o formulário para abertura de conta.')

print('<br> Senha provisória enviada')